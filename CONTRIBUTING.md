# How to contribute a research topic

Thank you for your interest. If there is anything you want to change about anything in this repository, feel free to reach out.  Pull requests are very welcome but by no means mandatory. 

If you are looking for a guided tour, watch [https://media.tuhh.de/e10/gabel/research-topics/video1_rundgang.mp4](https://media.tuhh.de/e10/gabel/research-topics/video1_rundgang.mp4)

If you feel overwhelmed and don't know where to start, watch this 5-min kickstart-video:

   [https://media.tuhh.de/e10/gabel/research-topics/topic-w-hedgedoc.mp4](https://media.tuhh.de/e10/gabel/research-topics/topic-w-hedgedoc.mp4)

And if you have doubts about compatibility with our institute page, let me show you what is possible.

![Demo Page](./img/webpage-example.png)

## Basic Template

Just *copy+paste* the source of the file [topic-template.md](https://gitlab.com/fabiangabel/research-topics-mat-tuhh/-/raw/master/topics/topic-template.md) or the following into a text-editor with [Markdown](https://guides.github.com/features/mastering-markdown/) syntax highlighting (personal recommendations below).

```
# The Title of our Impressive Reasearch Project

### Working Groups: aa, cm, dm, nm, st

### Collaborators (MAT): ataraz, cseifert, dclemens, dgallaun, druprecht, fboesch, fbuenger, fgabel, fhamann, hruan, hvoss, jangel, jdornemann, jfregin, jgrams, jgrossmann, jmeichsner, jpmzemke, jurizarna, kalbrecht, kklioba, kkruse, mjanssen, mlindner, mschulte, mwolkner, pbaasch, pgupta, rbeddig, rukena, sgoetschel, sleborne, sotten, tsaathoff, vgriem, vtrapp, wleinen, wmackens, ymogge

### Collaborators (External): [Vaughan Jones](https://as.vanderbilt.edu/math/bio/?who=vaughan-jones), [Terry Tao](https://terrytao.wordpress.com/about/)

## Description

This is about Math and contains some nice inline formulas like $\int_0^1 \sin(x) \mathrm{d}x$ and also some displayed formulas like

$$
\int_0^1 \sin(x) \mathrm{d}x
$$

It also contains some nice pictures


![Our Institute Logo](/img/logo_header_mat_de.png)


## References

[1] Integrability Methods in Harmonic Galois Theory. F. Gabel, M. Chebyshev, Z. B. Hadamard and N. Deligne. Available at [https://thatsmathematics.com/mathgen/](https://thatsmathematics.com/mathgen/paper.php?nameType%5B1%5D=custom&customName%5B1%5D=F.+Gabel&nameType%5B2%5D=famous&nameType%5B3%5D=famous&nameType%5B4%5D=famous&seed=126904073&format=pdf
)
``` 

Now, delete the working groups and collaborators that are not involved. 
The keys like `ataraz`, `cseifert`, etc. are taken from the URL of our institute webpages:

![Staff-Key](/img/keyname.png)

### Editing Markdown

If the above template lacks a particular feature you want to use, check out this short [Markdown Guide](https://guides.github.com/features/mastering-markdown/).


#### Online: HedgeDoc @ TUHH

Just LogIn at https://writemd.rz.tuhh.de/ and create a new note. Paste all of the above into a document and work on it.

![Our Institute Logo](/img/hedgedoc-demo.png)

#### Online: HedgeDoc @ HedgeDoc

Same as **HedgeDoc @ TUHH**. Just visit https://demo.hedgedoc.org/ .

#### Offline: VSCodium (MIT license)

Download [VSCodium](https://vscodium.com/)  and install the [Mathpix Markdown](https://github.com/mathpix/vscode-mathpix-markdown.git) extension. 

Render your document with `<CTRL>+<SHIFT>+V`

![VS Code](/img/vscodium-demo.png)

#### Offline: VSCode (by Microsoft)

Download [VSCode](https://code.visualstudio.com/) and install the [Markdown+Math](https://github.com/goessner/mdmath) extension. Exactly the same as VSCodium but with Microsoft's branding. See [here](https://dev.to/0xdonut/why-and-how-you-should-to-migrate-from-visual-studio-code-to-vscodium-j7d).

As for VSCodium, render your document with `<CTRL>+<SHIFT>+V`

## Publishing your File

Choose whatever you feel comfortable with: You can forward your topic.md to me via

* Mail (see below, not elegant but simple)
* TUHH-cloud (check your mails for the link) (still not elegant but simple)
* GitLab merge request at:  https://gitlab.com/fabiangabel/research-topics-mat-tuhh  (not as simple but elegant)


## Contact

* Just email [Fabian Gabel](mailto:fabian.gabel@tuhh.de) or 
* Message [@cfg0846](https://communicating.tuhh.de/entrance/messages/@cfg0846) on [TUHH-Mattermost](https://communicating.tuhh.de/)
* Join the discussion in our Mattermost [Research Channel](https://communicating.tuhh.de/signup_user_complete/?id=r3jajdid77dqb8pc81a6nu9g4e)

