# Fast Strategies in Waiter-Client Games

### Working Groups: dm

### Collaborators (MAT): dclemens, fhamann, pgupta, ymogge

### Collaborators (External): Alexander Haupt, [Mirjana Mikalački](https://people.dmi.uns.ac.rs/~mima/)

## Description

Waiter-Client games are played on some hypergraph $(X,\mathcal{F})$, where $\mathcal{F}$ denotes the family of winning sets. For some bias $b$, during each round of such a game Waiter offers to Client $b+1$ elements of $X$, of which Client claims one for himself while the rest go to Waiter. Proceeding like this Waiter wins the game if she forces Client to claim all the elements of any winning set from $\mathcal{F}$. In this paper we study fast strategies for several Waiter-Client games played on the edge set of the complete graph, i.e. $X=E(K_n)$, in which the winning sets are perfect matchings, Hamilton cycles, pancyclic graphs, fixed spanning trees or factors of a given graph.


## References

[1] [D. Clemens, P. Gupta, F. Hamann, A. Haupt, M. Mikalački, and Y. Mogge. Fast Strategies in Waiter-Client Games, The Electronic Journal of Combinatorics 27(3) (2020), P3.57](https://doi.org/10.37236/9451 )
