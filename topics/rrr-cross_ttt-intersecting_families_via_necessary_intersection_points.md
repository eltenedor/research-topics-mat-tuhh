# $r$-cross $t$-intersecting families via necessary intersection points

### Working Groups: dm

### Collaborators (MAT): pgupta, ymogge

### Collaborators (External): Simón Piga, [Bjarne Schülke](https://www.math.uni-hamburg.de/home/schuelke/)

## Description

Given integers $r\geq 2$ and $n,t\geq 1$ we call families $\mathcal{F}_1,\dots,\mathcal{F}_r\subseteq\mathscr{P}([n])$ $r$-cross $t$-intersecting if for all $F_i\in\mathcal{F}_i$, $i\in[r]$, we have $\vert\bigcap_{i\in[r]}F_i\vert\geq t$. We obtain a strong generalisation of the classic Hilton-Milner theorem on cross intersecting families. In particular, we determine the maximum of $\sum_{j\in [r]}\vert\mathcal{F}_j\vert$ for $r$-cross $t$-intersecting families in the cases when these are $k$-uniform families or arbitrary subfamilies of $\mathscr{P}([n])$. Only some special cases of these results had been proved before. We obtain the aforementioned theorems as instances of a more general result that considers measures of $r$-cross $t$-intersecting families. This also provides the maximum of $\sum_{j\in [r]}\vert\mathcal{F}_j\vert$ for families of possibly mixed uniformities $k_1,\ldots,k_r$. 


## References

[1] [P. Gupta, Y. Mogge, S. Piga, and B. Schülke. $r$-cross $t$-intersecting families via necessary intersection points, arXiv:2010.11928 (2020)](https://arxiv.org/abs/2010.11928)