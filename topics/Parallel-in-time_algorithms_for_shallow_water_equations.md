# Parallel-in-time algorithms for shallow water equations

### Working Groups: cm

### Collaborators (MAT): druprecht, jangel,  sgoetschel

### Collaborators (External): [Jörn Behrens](https://www.math.uni-hamburg.de/home/behrens/)

## Description

The prediction and thus modelling of tsunamis has been of interest for many years. Tsunami waves can be modelled by the shallow water equations. In my research, I am focusing on the numerical solution of one-dimensional shallow water equations. In order to make computations faster, the time integration is parallelised using Parareal or other parallel-in-time algorithms, where in space, discontinuous Galerkin methods are being used.
