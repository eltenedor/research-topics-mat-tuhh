# Research Interests

This page hopefully features the past, current and future research interests of our institute.

You browse this webpage either by clicking on a **research topic**, a **working group** or a particular **researcher**.

If you are looking for a guided tour, watch [https://media.tuhh.de/e10/gabel/research-topics/video1_rundgang.mp4](https://media.tuhh.de/e10/gabel/research-topics/video1_rundgang.mp4)

The webpage is currently delivered to [https://fabiangabel.gitlab.io/research-topics-mat-tuhh](https://fabiangabel.gitlab.io/research-topics-mat-tuhh).

The corresponding GitLab repository is hosted at [https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh) and push-mirrored to [https://gitlab.com/fabiangabel/research-topics-mat-tuhh](https://gitlab.com/fabiangabel/research-topics-mat-tuhh)

If you want to contribute a research topic, see the [CONTRIBUTING.md](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/blob/master/CONTRIBUTING.md).

In order to display the potential of this page, we are all part of the [template research topic](https://fabiangabel.gitlab.io/research-topics-mat-tuhh/topic-template.html).

%%%%%%%% THE FOLLOWING CONTENT IS PROCESSED AUTOMATICALLY. DON'T CHANGE THE FILE BELOW THIS LINE! %%%%%%%%

### Working Groups: aa, cm, dm, nm, st

### Collaborators (MAT): ataraz, cseifert, dclemens, dgallaun, druprecht, fboesch, fbuenger, fgabel, fhamann, hruan, hvoss, jangel, jdornemann, jfregin, jgrams, jgrossmann, jmeichsner, jpmzemke, jurizarna, kalbrecht, kklioba, kkruse, mjanssen, mlindner, mschulte, mwolkner, pbaasch, pgupta, rbeddig, rukena, sgoetschel, sleborne, sotten, tsaathoff, vgriem, vtrapp, wleinen, wmackens, ymogge

## Research Topics
