#!/bin/bash

baseurl="https://www.mat.tuhh.de"

for f in *.html
do 
  sed -i 's;windows-1252;utf-8;g' $f
  sed -i 's;iso-8859-1;utf-8;g' $f
#adapt paths
  #css
  sed -i 's;/include/css;css;g' $f 
  #images
  sed -i 's;/include/images;img;g' $f
  #/index
  sed -i 's;"/index.html;"index.html;' $f
  #external links group
  for string in home forschung informationen lehre studium veranstaltungen
  do
    sed -i "s;\"/$string;\"$baseurl/$string;" $f
  done

  for string in home forschung 
  do
    sed -i 's;'\'"/$string;"\'"$baseurl/$string;" $f
  done
  #javascript
    # https://cdnjs.com and 
    sed -i "s;/include/javascript/jquery.js;https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js;" $f
    sed -i "s;/include/javascript/hoverIntent.js;https://cdnjs.cloudflare.com/ajax/libs/jquery.hoverintent/1.10.1/jquery.hoverIntent.js;" $f
    sed -i "s;/include/javascript/superfish.js;https://cdnjs.cloudflare.com/ajax/libs/superfish/1.7.10/js/superfish.js;" $f

    # mat.tuhh.de
    sed -i "s;/include/javascript/menu-init.js;https://www.mat.tuhh.de/include/javascript/menu-init.js;" $f
    sed -i "s;/include/javascript/toggleme.js;https://www.mat.tuhh.de/include/javascript/toggleme.js;" $f
    sed -i "s;/include/javascript/mathjax-init.js;https://www.mat.tuhh.de/include/javascript/toggleme.js;" $f
    sed -i "s;/include/javascript/jquery.quicksilver.js;https://www.mat.tuhh.de/include/javascript/toggleme.js;" $f
    sed -i "s;/include/javascript/jquery.simpleFAQ.js;https://www.mat.tuhh.de/include/javascript/jquery.simpleFAQ.js;" $f
done
