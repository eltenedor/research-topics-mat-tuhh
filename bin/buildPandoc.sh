#!/bin/bash

cd $RESEARCH_ROOT/build
cp -r $RESEARCH_ROOT/css $RESEARCH_ROOT/build
for f in *.md
do
    pandoc --quiet --standalone $f -o  "${f%.md}".html \
    --mathjax \
    -c css/base.css \
    -c css/extra.css \
    -c "css/print.css" \
    -c "css/superfish.css" \
    -c "css/superfish-vertical.css" \
    -c "css/base_mode.css"
done
