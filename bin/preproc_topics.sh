#!/bin/bash
# processing command line arguments
if [ $# -lt 1 ]
then
    RESEARCH_BUILD="$RESEARCH_ROOT/build"
    echo -e "No building directory was specified."
else
    RESEARCH_BUILD="$1"/build
fi
echo -e "Building into directory $RESEARCH_BUILD ..."
rm -rf $RESEARCH_BUILD 
mkdir -p $RESEARCH_BUILD
baseurl='https://www.mat.tuhh.de'

#build index.html
#replace working group
cp $RESEARCH_ROOT/index.md $RESEARCH_BUILD/index.md
$RESEARCH_ROOT/bin/preproc_wg.sh $RESEARCH_BUILD/index.md $RESEARCH_BUILD >> $RESEARCH_BUILD/build.log 2>&1

#replace staff names
$RESEARCH_ROOT/bin/preproc_staff.sh $RESEARCH_BUILD/index.md $RESEARCH_BUILD >> $RESEARCH_BUILD/build.log 2>&1

#finalize file
sed -i "s;\](/img/;\](./img/;g" -e '/PROCESSED AUTOMATICALLY/ d' $RESEARCH_BUILD/index.md


#build rest of the topics
for f in $RESEARCH_ROOT/topics/*.md
do
    filename=$(basename -- "$f")
    #make output copy
    echo "Preprocessing $filename..."
    cp $f $RESEARCH_BUILD/$filename

    #extract title of topic
    title=`$RESEARCH_ROOT/bin/extract_title.sh $RESEARCH_BUILD/$filename`
    echo "Preprocessing topic $title"

    # append research topic to index
    echo -e "\n$title\n" >>  $RESEARCH_BUILD/index.md

    #replace working group
    $RESEARCH_ROOT/bin/preproc_wg.sh $RESEARCH_BUILD/$filename $RESEARCH_BUILD >> $RESEARCH_BUILD/build.log 2>&1

    #replace staff names
    $RESEARCH_ROOT/bin/preproc_staff.sh $RESEARCH_BUILD/$filename $RESEARCH_BUILD >> $RESEARCH_BUILD/build.log 2>&1

    #finalize file
    sed -i "s;\](/img/;\](./img/;g" $RESEARCH_BUILD/$filename
done

echo "Finished preprocessing topics."